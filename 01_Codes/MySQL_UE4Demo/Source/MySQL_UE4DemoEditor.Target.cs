// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class MySQL_UE4DemoEditorTarget : TargetRules
{
	public MySQL_UE4DemoEditorTarget(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Editor;

        ExtraModuleNames.AddRange(new string[] { "MySQL_UE4Demo" });
    }
}
