﻿// MySQL_Client_Test_01.cpp : 定义控制台应用程序的入口点。
// Calling Stored Procedures with Statement Objects:https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-statements.html
//

#include "stdafx.h"

#include <iostream>
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn\statement.h>
#include <cppconn\prepared_statement.h>
#include <vector>

using namespace std;
/** For MySQL UTF8 */
// #pragma execution_character_set("utf-8") 

#define EXAMPLE_HOST u8"localhost"
#define EXAMPLE_USER u8"root"
#define EXAMPLE_PASS u8"123456"
#define EXAMPLE_DB u8"world"

int MySQLConnector_Test00(int argc, const char **argv);
int MySQLConnector_Test01();
int MySQLConnector_Test02();
int MySQLConnector_Test03();
int MySQLConnector_Test04();
int MySQLConnector_Test05();
int MySQLConnector_Test06(int argc, const char **argv);
int MySQLConnector_Test07(int argc, const char **argv);
int MySQLConnector_Test08(int argc, const char **argv);
int MySQLConnector_Test09(int argc, const char **argv);
int MySQLConnector_Test10(int argc, const char **argv);
int MySQLConnector_Test11(int argc, const char **argv);

int main(int argc, const char* argv[])
{
	MySQLConnector_Test00(argc, argv);
	MySQLConnector_Test01();
	MySQLConnector_Test02();
	MySQLConnector_Test03();
	MySQLConnector_Test04();
	MySQLConnector_Test05();
	MySQLConnector_Test06(argc, argv);
	MySQLConnector_Test07(argc, argv);
	MySQLConnector_Test08(argc, argv);
	MySQLConnector_Test09(argc, argv);
	MySQLConnector_Test10(argc, argv);
	MySQLConnector_Test11(argc, argv);

	getchar();
}

/*
*	MySQLConnector C++ Tutorial Framework
*/
int MySQLConnector_Test00(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial framework..." << endl;
	cout << endl;

	try {

		/* INSERT TUTORIAL CODE HERE! */

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	Statement Execute Test.
*/
int MySQLConnector_Test01()
{
	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial: Statement Execute Test." << endl;
	cout << endl;

	try
	{
		sql::mysql::MySQL_Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;

		driver = sql::mysql::get_mysql_driver_instance();
		con = driver->connect(EXAMPLE_HOST, EXAMPLE_USER, EXAMPLE_PASS);

		stmt = con->createStatement();
		std::string dataBase = "USE ";
		bool isClosed = con->isClosed();
		cout << "1. IsClosed: " << isClosed << endl;
		stmt->execute(dataBase.append(EXAMPLE_DB));
		stmt->execute(u8"DROP TABLE IF EXISTS test");
		stmt->execute(u8"CREATE TABLE test(id INT, label VARCHAR(20) CHARACTER SET utf8)");
		stmt->execute(u8"INSERT INTO test(id, label) VALUES (1, 'Hi!你好！')");
		isClosed = con->isClosed();
		cout << "2. IsClosed: " << isClosed << endl;

		delete stmt;
		delete con;
	}
	catch (exception e)
	{
		cout << "Exception: " << e.what() << endl;
	}
	return EXIT_SUCCESS;
}

/*
*	Statement ExecuteQuery Test.
*/
int MySQLConnector_Test02()
{
	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial: Statement ExecuteQuery Test." << endl;
	cout << endl;

	try 
	{
		sql::mysql::MySQL_Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet *res;
		sql::ConnectOptionsMap connection_properties;

		connection_properties["hostName"] = EXAMPLE_HOST;
		connection_properties["userName"] = EXAMPLE_USER;
		connection_properties["password"] = EXAMPLE_PASS;
		connection_properties["schema"] = EXAMPLE_DB;
		connection_properties["port"] = 3306;
		connection_properties["OPT_RECONNECT"] = true;

		driver = sql::mysql::get_mysql_driver_instance();
		//con = driver->connect(EXAMPLE_HOST, EXAMPLE_USER, EXAMPLE_PASS);
		con = driver->connect(connection_properties);

		if (nullptr == con)
		{
			cout << "连接数据库失败！" << endl;
		}

		stmt = con->createStatement();
		// std::string dataBase = "USE ";
		// stmt->execute(dataBase.append(EXAMPLE_DB));
		res = stmt->executeQuery(u8"SELECT id, label From test ORDER BY id ASC");
		while (res->next())
		{
			cout << "id= " << res->getInt(1); // getInt(1) returns the first column
											  // or column names for accessing results.
											  // The latter is recommended.
			cout << ", label= " << res->getString(u8"label") << "'" << endl;
		}
		delete res;
		delete stmt;
		delete con;
	}
	catch (exception e)
	{
		cout << "Exception: " << e.what() << endl;
	}
	return EXIT_SUCCESS;
}

/*
*	PepareStatement Test.
*/
int MySQLConnector_Test03()
{
	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial: PepareStatement Test." << endl;
	cout << endl;

	try
	{
		sql::mysql::MySQL_Driver *driver;
		sql::Connection *con;

		sql::PreparedStatement  *prep_stmt;

		driver = sql::mysql::get_mysql_driver_instance();
		con = driver->connect(EXAMPLE_HOST, EXAMPLE_USER, EXAMPLE_PASS);
		/* Connect to the MySQL test database */
		con->setSchema(EXAMPLE_DB);

		prep_stmt = con->prepareStatement(u8"INSERT INTO test(id, label) VALUES (?, ?)");

		prep_stmt->setInt(1, 1);
		prep_stmt->setString(2, "a");
		prep_stmt->execute();

		prep_stmt->setInt(1, 2);
		prep_stmt->setString(2, "Hello!");
		prep_stmt->execute();

		delete prep_stmt;
		delete con;
	}
	catch(sql::SQLException &e) {
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;
	}
	return EXIT_SUCCESS;
}

/*
*	Statement ExecuteQuery Test.
*/
int MySQLConnector_Test04()
{
	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial: Statement ExecuteQuery Test." << endl;
	cout << endl;

	try 
	{
		sql::Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet *res;

		/* Create a connection */
		driver = get_driver_instance();
		con = driver->connect(EXAMPLE_HOST, EXAMPLE_USER, EXAMPLE_PASS);
		/* Connect to the MySQL test database */
		con->setSchema(EXAMPLE_DB);

		stmt = con->createStatement();
		res = stmt->executeQuery(u8"SELECT 'Hello World!' AS _message");
		while (res->next()) {
			cout << "\t... MySQL replies: ";
			/* Access column data by alias or column name */
			cout << res->getString(u8"_message") << endl;
			cout << "\t... MySQL says it again: ";
			/* Access column data by numeric offset, 1 is the first column */
			cout << res->getString(1) << endl;
		}
		delete res;
		delete stmt;
		delete con;

	}
	catch (sql::SQLException &e) {
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;
	}

	cout << endl;

	return EXIT_SUCCESS;
}

/*
*	Statement ExecuteQuery Test.
*/
int MySQLConnector_Test05()
{
	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial: Statement ExecuteQuery Test. Let's have MySQL count from 10 to 1..." << endl;
	cout << endl;

	try {
		sql::Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet *res;
		sql::PreparedStatement *pstmt;

		/* Create a connection */
		driver = get_driver_instance();
		con = driver->connect(EXAMPLE_HOST, EXAMPLE_USER, EXAMPLE_PASS);
		/* Connect to the MySQL test database */
		con->setSchema(EXAMPLE_DB);

		stmt = con->createStatement();
		stmt->execute(u8"DROP TABLE IF EXISTS test2");
		stmt->execute(u8"CREATE TABLE test2(id INT)");
		delete stmt;

		/* '?' is the supported placeholder syntax */
		pstmt = con->prepareStatement(u8"INSERT INTO test2(id) VALUES (?)");
		for (int i = 1; i <= 10; i++) {
			pstmt->setInt(1, i);
			pstmt->executeUpdate();
		}
		delete pstmt;

		/* Select in ascending order */
		pstmt = con->prepareStatement(u8"SELECT id FROM test ORDER BY id ASC");
		res = pstmt->executeQuery();

		/* Fetch in reverse = descending order! */
		res->afterLast();
		while (res->previous())
			cout << "\t... MySQL counts: " << res->getInt(u8"id") << endl;
		delete res;

		delete pstmt;
		delete con;

	}
	catch (sql::SQLException &e) {
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;
	}

	cout << endl;

	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a Statement for a Stored Procedure That Returns No Result
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-statement-no-result.html
*/
int MySQLConnector_Test06(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a Statement for a Stored Procedure That Returns No Result" << endl;
	cout << endl;

	try {

		sql::Driver* driver = get_driver_instance();
		std::auto_ptr<sql::Connection> con(driver->connect(url, user, pass));
		con->setSchema(database);
		std::auto_ptr<sql::Statement> stmt(con->createStatement());
		sql::ResultSet *res;

		// We need not check the return value explicitly. If it indicates
		// an error, Connector/C++ generates an exception.
		stmt->execute(u8"delete from country where code='ATL'");
		/*
		DELIMITER /
			CREATE PROCEDURE add_country(IN country_code CHAR(3),
				IN country_name CHAR(52),
				IN continent_name CHAR(30))
			BEGIN
			INSERT INTO Country(Code, Name, Continent)
			VALUES(country_code, country_name, continent_name);
		END
			/
		DELIMITER;
		*/
		stmt->execute(u8"CALL add_country('ATL', 'Atlantis', 'North America')");
		res = stmt->executeQuery(u8"SELECT Code, Name, Continent FROM Country WHERE Code='ATL'");
		while (res->next()) {
			cout << "Code: " << res->getString(u8"Code") << "; Name: " << res->getString(u8"Name") << "; Continent: " << res->getString(u8"Continent");
		}

		delete res;
	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a Statement for a Stored Procedure That Returns an Output Parameter
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-statement-out-parameter.html
*/
int MySQLConnector_Test07(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a Statement for a Stored Procedure That Returns an Output Parameter" << endl;
	cout << endl;

	try {

		sql::Driver* driver = get_driver_instance();
		std::auto_ptr<sql::Connection> con(driver->connect(url, user, pass));
		con->setSchema(database);
		std::auto_ptr<sql::Statement> stmt(con->createStatement());

		stmt->execute(u8"CALL get_pop('Uganda', @pop)");

		std::auto_ptr<sql::ResultSet> res(stmt->executeQuery(u8"SELECT @pop AS _reply"));
		while (res->next())
			cout << "Population of Uganda: " << res->getString(u8"_reply") << endl;

		stmt->execute(u8"CALL get_pop_continent('Asia', @pop)");

		res.reset(stmt->executeQuery(u8"SELECT @pop AS _reply"));
		while (res->next())
			cout << "Population of Asia: " << res->getString(u8"_reply") << endl;

		stmt->execute(u8"CALL get_pop_world(@pop)");

		res.reset(stmt->executeQuery(u8"SELECT @pop AS _reply"));
		while (res->next())
			cout << "Population of World: " << res->getString(u8"_reply") << endl;

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a Statement for a Stored Procedure That Returns a Result Set
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-statement-with-result.html
*/
int MySQLConnector_Test08(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a Statement for a Stored Procedure That Returns a Result Set" << endl;
	cout << endl;

	try {

		sql::Driver* driver = get_driver_instance();
		std::auto_ptr<sql::Connection> con(driver->connect(url, user, pass));
		con->setSchema(database);
		std::auto_ptr<sql::Statement> stmt(con->createStatement());

		stmt->execute(u8"CALL get_data()");
		std::auto_ptr< sql::ResultSet > res;
		do {
			cout << "=====New ResultSet====="<<endl;
			res.reset(stmt->getResultSet());
			while (res->next()) {
				cout << "Name: " << res->getString(u8"Name")
					<< " Population: " << res->getInt(u8"Population")
					<< endl;
			}
		} while (stmt->getMoreResults());

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a PreparedStatement for a Stored Procedure That Returns No Result
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-prepared-statement-no-result.html
*/
int MySQLConnector_Test09(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a PreparedStatement for a Stored Procedure That Returns No Result" << endl;
	cout << endl;

	try {

		vector<string> code_vector;
		code_vector.push_back("SLD");
		code_vector.push_back("DSN");
		code_vector.push_back("ATL");

		vector<string> name_vector;
		name_vector.push_back("Sealand");
		name_vector.push_back("Disneyland");
		name_vector.push_back("Atlantis");

		vector<string> cont_vector;
		cont_vector.push_back("Europe");
		cont_vector.push_back("North America");
		cont_vector.push_back("Oceania");

		sql::Driver * driver = get_driver_instance();

		std::auto_ptr< sql::Connection > con(driver->connect(url, user, pass));
		con->setSchema(database);

		std::auto_ptr< sql::PreparedStatement >  pstmt;

		pstmt.reset(con->prepareStatement(u8"CALL add_country(?,?,?)"));
		for (int i = 0; i<3; i++)
		{
			cout << "Inserting a new line:" << i << endl;
			pstmt->setString(1, code_vector[i]);
			pstmt->setString(2, name_vector[i]);
			pstmt->setString(3, cont_vector[i]);

			pstmt->execute();
		}

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a PreparedStatement for a Stored Procedure That Returns an Output Parameter
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-prepared-statement-out-parameter.html
*/
int MySQLConnector_Test10(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a PreparedStatement for a Stored Procedure That Returns an Output Parameter" << endl;
	cout << endl;

	try {

		vector<string> cont_vector;
		cont_vector.push_back("Europe");
		cont_vector.push_back("North America");
		cont_vector.push_back("Oceania");

		sql::Driver * driver = get_driver_instance();

		std::auto_ptr< sql::Connection > con(driver->connect(url, user, pass));
		con->setSchema(database);

		std::auto_ptr< sql::Statement > stmt(con->createStatement());
		std::auto_ptr< sql::PreparedStatement >  pstmt;
		std::auto_ptr< sql::ResultSet > res;

		pstmt.reset(con->prepareStatement(u8"CALL get_pop_continent(?,@pop)"));

		for (int i = 0; i<3; i++)
		{
			pstmt->setString(1, cont_vector[i]);
			pstmt->execute();
			res.reset(stmt->executeQuery(u8"SELECT @pop AS _population"));
			while (res->next())
				cout << "Population of "
				<< cont_vector[i]
				<< " is "
				<< res->getString(u8"_population") << endl;
		}

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}

/*
*	MySQLConnector C++ Tutorial
*	Using a PreparedStatement for a Stored Procedure That Returns a Result Set
*	https://dev.mysql.com/doc/connector-cpp/en/connector-cpp-tutorials-stored-routines-prepared-statement-with-result.html
*/
int MySQLConnector_Test11(int argc, const char **argv)
{
	string url(argc >= 2 ? argv[1] : EXAMPLE_HOST);
	const string user(argc >= 3 ? argv[2] : EXAMPLE_USER);
	const string pass(argc >= 4 ? argv[3] : EXAMPLE_PASS);
	const string database(argc >= 5 ? argv[4] : EXAMPLE_DB);

	cout << "=================================================================================================" << endl;
	cout << "File: " << __FILE__;
	cout << "  Function: " << __FUNCTION__ << "  Line:" << __LINE__ << endl;
	cout << "Connector/C++ tutorial:Using a PreparedStatement for a Stored Procedure That Returns an Output Parameter" << endl;
	cout << endl;

	try {

		sql::Driver * driver = get_driver_instance();

		std::auto_ptr< sql::Connection > con(driver->connect(url, user, pass));
		con->setSchema(database);

		std::auto_ptr< sql::PreparedStatement >  pstmt;
		std::auto_ptr< sql::ResultSet > res;

		pstmt.reset(con->prepareStatement(u8"CALL get_data()"));
		res.reset(pstmt->executeQuery());

		for (;;)
		{
			while (res->next()) {
				cout << "Name: " << res->getString(u8"Name")
					<< " Population: " << res->getInt(u8"Population")
					<< endl;
			}
			if (pstmt->getMoreResults())
			{
				res.reset(pstmt->getResultSet());
				continue;
			}
			break;
		}

	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:
		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		cout << "#####################" << endl;
		cout << "# Exception: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# Message: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		cout << "#####################" << endl;

		return EXIT_FAILURE;
	}

	cout << "Done." << endl;
	return EXIT_SUCCESS;
}